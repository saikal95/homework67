import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { MainPageofcalsComponent } from './main-pageofcals/main-pageofcals.component';
import { EditMealComponent } from './main-pageofcals/edit-meal/edit-meal.component';
import {FormsModule} from "@angular/forms";
import {HttpClientModule} from "@angular/common/http";
import {CaloriesService} from "./shared/calories.service";
import { MealsComponent } from './main-pageofcals/meals/meals.component';
import { MealItemComponent } from './main-pageofcals/meals/meal-item/meal-item.component';

@NgModule({
  declarations: [
    AppComponent,
    MainPageofcalsComponent,
    EditMealComponent,
    MealsComponent,
    MealItemComponent,
  ],
  imports: [
    BrowserModule,
    FormsModule,
    HttpClientModule,
    AppRoutingModule,
  ],
  providers: [CaloriesService],
  bootstrap: [AppComponent]
})
export class AppModule { }
