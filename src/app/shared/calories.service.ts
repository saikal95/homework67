import {Subject} from "rxjs";
import {Meal} from "./meal.model";
import {HttpClient} from "@angular/common/http";
import {map, tap} from "rxjs/operators";
import {Injectable} from "@angular/core";

@Injectable()
export class CaloriesService {
  caloriesChange = new Subject <Meal[]>();
  caloriesFetching = new Subject<boolean>();
  caloriesUploading = new Subject <boolean>();
  caloryRemoving = new Subject <boolean>();
  calorie: number = 0;

  private meals: Meal[] = [
  ];


  constructor(private http: HttpClient) {}

  getMeals() {
    return this.meals.slice();
  }

  fetchMeals(){
    this.caloriesFetching.next(true);
    this.http.get<{[id:string]:Meal}>('https://another-plovo-default-rtdb.firebaseio.com/meals.json')
      .pipe(map (result => {
        return Object.keys(result).map(id=> {
          const mealData = result[id];
          return new Meal(
            id,
            mealData.mealType,
            mealData.description,
            mealData.cal,
          );
        });
      }))
      .subscribe(meals=>{
        this.meals = meals;
        this.caloriesChange.next(this.meals.slice());
        this.caloriesFetching.next(false);
      }, () => {
        this.caloriesFetching.next(false);
      })
  }

  addMeal(meal: Meal) {
    const body = {
      mealType: meal.mealType,
      description: meal.description,
       cal: meal.cal,
    };

    this.caloriesUploading.next(true);

    return this.http.post('https://another-plovo-default-rtdb.firebaseio.com/meals.json', body).pipe(
      tap(() => {
        this.caloriesUploading.next(false);
      }, () => {
        this.caloriesUploading.next(false);
      })
    );
  }

  editMeal(meal: Meal){
    this.caloriesUploading.next(true);

    const body = {
      mealType: meal.mealType,
      description: meal.description,
      cal: meal.cal,
    }

    return this.http.put(`https://another-plovo-default-rtdb.firebaseio.com/meals/${meal.id}.json`, body).pipe(
      tap(() => {
        this.caloriesUploading.next(false);
      }, () => {
        this.caloriesUploading.next(false);
      })
    );
  }


  removeMeal(id: string) {
    this.caloryRemoving.next(true);
    return this.http.delete(`https://another-plovo-default-rtdb.firebaseio.com/meals/${id}.json`).pipe(
      tap(() => {
        this.caloryRemoving.next(false);
      }, () => {
        this.caloryRemoving.next(false);
      })
    );
  }


  fetchMeal(id:string){
    return this.http.get<Meal | null>(`https://another-plovo-default-rtdb.firebaseio.com/meals/${id}.json`).pipe(map(
      result => {
        if(!result){
          return null;
        }
        return new Meal (
          id, result.mealType,result.description, result.cal);
      }
    ))
  }

  }


