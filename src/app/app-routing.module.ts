import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import {MainPageofcalsComponent} from "./main-pageofcals/main-pageofcals.component";
import {EditMealComponent} from "./main-pageofcals/edit-meal/edit-meal.component";
import {MealsComponent} from "./main-pageofcals/meals/meals.component";
import {MealItemComponent} from "./main-pageofcals/meals/meal-item/meal-item.component";
import {MealResolverService} from "./main-pageofcals/meals/meal.resolver";

const routes: Routes = [
  {path: '', component: MainPageofcalsComponent},
  {path: 'meals', component: MainPageofcalsComponent},
  {path: 'meals/new', component: EditMealComponent},
  {path: ':id', component: MealItemComponent},
  {path: 'meals/:id', component: EditMealComponent,
    resolve:{
      meal: MealResolverService
    }
  },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
