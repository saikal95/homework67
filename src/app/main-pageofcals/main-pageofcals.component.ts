import { Component, OnInit } from '@angular/core';
import {Router, RouterLink} from "@angular/router";
import {Meal} from "../shared/meal.model";
import {CaloriesService} from "../shared/calories.service";

@Component({
  selector: 'app-main-pageofcals',
  templateUrl: './main-pageofcals.component.html',
  styleUrls: ['./main-pageofcals.component.css']
})
export class MainPageofcalsComponent implements OnInit {

  meals: Meal[] = [];
  calorie = 0;

  constructor(private router: Router, private calorieService: CaloriesService) {
  }

  ngOnInit(): void {
  }

  navigatoAdd() {
    void this.router.navigate(['meals/new']);
  }


}
