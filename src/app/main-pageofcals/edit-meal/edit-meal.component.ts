import {Component, OnDestroy, OnInit, ViewChild} from '@angular/core';
import {NgForm} from "@angular/forms";
import {Subscription} from "rxjs";
import {Meal} from "../../shared/meal.model";
import {ActivatedRoute, Router} from "@angular/router";
import {CaloriesService} from "../../shared/calories.service";

@Component({
  selector: 'app-edit-meal',
  templateUrl: './edit-meal.component.html',
  styleUrls: ['./edit-meal.component.css']
})
export class EditMealComponent implements OnInit,OnDestroy {

  @ViewChild('f')mealForm! : NgForm;

  isEdit = false;
  editedId = '';

  isUploading = false;
  caloriesUploadingSubscription!: Subscription;

  constructor(private calorieService: CaloriesService,
              private  router: Router,
              private route: ActivatedRoute) {}

  ngOnInit() {
    this.caloriesUploadingSubscription = this.calorieService.caloriesUploading.subscribe((isUploading:boolean)=> {
      this.isUploading = isUploading;
    });

    this.route.data.subscribe(data => {
      const meal = <Meal | null>data.meal;

      if (meal) {
        this.isEdit = true;
        this.editedId = meal.id;
        this.setFormValue({
          mealType: meal.mealType,
          description: meal.description,
          cal: meal.cal,
        })
      } else {
        this.isEdit = false;
        this.editedId = '';
        this.setFormValue({
          mealType: '',
          description: '',
          cal: 0,
        })
      }
    });
  }

  setFormValue(value: {[key: string]: any}){
    setTimeout(() => {
      this.mealForm.form.setValue(value);
    });
  }


  saveMeal() {
    const id = this.editedId
    const meal = new Meal (id, this.mealForm.value.mealType, this.mealForm.value.description, this.mealForm.value.cal);
    const next = () => {this.calorieService.fetchMeals();
    };

    if(this.isEdit){
      this.calorieService.editMeal(meal).subscribe(next);
    } else {
      this.calorieService.addMeal(meal).subscribe(next);
    }

      void this.router.navigate(['meals']);

  }


  ngOnDestroy() {
    this.caloriesUploadingSubscription.unsubscribe();
  }



}
