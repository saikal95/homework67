import {Meal} from "../../shared/meal.model";
import {ActivatedRouteSnapshot, Resolve, Router, RouterStateSnapshot} from "@angular/router";
import {Injectable} from "@angular/core";
import {CaloriesService} from "../../shared/calories.service";
import {EMPTY, Observable, of} from "rxjs";
import {mergeMap} from "rxjs/operators";

@Injectable({
  providedIn: 'root'
})
export class MealResolverService implements  Resolve<Meal>{

  constructor(private calorieService : CaloriesService, private router: Router) { }

  resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<Meal> | Observable<never>  {
    const mealId = <string>route.params['id'];
    return this.calorieService.fetchMeal(mealId).pipe(mergeMap(meal=> {
      if(meal){
        return of(meal)
      }

      void this.router.navigate(['/']);
      return EMPTY;
    }));
  }




}
