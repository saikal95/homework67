import {Component, Input, OnDestroy, OnInit} from '@angular/core';
import {Meal} from "../../../shared/meal.model";
import {CaloriesService} from "../../../shared/calories.service";
import {ActivatedRoute, Router} from "@angular/router";
import {Subscription} from "rxjs";

@Component({
  selector: 'app-meal-item',
  templateUrl: './meal-item.component.html',
  styleUrls: ['./meal-item.component.css']
})
export class MealItemComponent  implements  OnInit, OnDestroy{
  @Input() meal!: Meal;
  isRemoving = false;
  calorieRemovingSubscription!: Subscription;

  constructor(private route: ActivatedRoute,
              private router: Router,
              private calorieService: CaloriesService
  ) {}

  ngOnInit(): void {
    this.calorieRemovingSubscription = this.calorieService.caloryRemoving.subscribe((isRemoving:boolean) => {
      this.isRemoving = isRemoving;
    })

  }

  onRemove(id: string) {
    this.calorieService.removeMeal(id).subscribe(() => {
      this.calorieService.fetchMeals();

      void this.router.navigate(['meals']);
    });

  }

  ngOnDestroy() {
    this.calorieRemovingSubscription.unsubscribe()
  }







}
