import {Component, OnDestroy, OnInit} from '@angular/core';
import {Meal} from "../../shared/meal.model";
import {Subscription} from "rxjs";
import {CaloriesService} from "../../shared/calories.service";
import {parse} from "@angular/compiler/src/render3/view/style_parser";


@Component({
  selector: 'app-meals',
  templateUrl: './meals.component.html',
  styleUrls: ['./meals.component.css']
})
export class MealsComponent implements OnInit,OnDestroy {
  meals!: Meal[];
  loading = false;
  calorieChangeSubscription! : Subscription;
  calorieFetchingSubscription! : Subscription;
  cal : number =0;

  constructor(private calorieService: CaloriesService) {}

  ngOnInit() {
    this.meals = this.calorieService.getMeals();
    this.calorieChangeSubscription = this.calorieService.caloriesChange.subscribe((meals:Meal[]) => {
      this.meals = meals;
    })
    this.calorieFetchingSubscription =  this.calorieService.caloriesFetching.subscribe((isFetching:boolean) => {
      this.loading = isFetching;
    })
    this.calorieService.fetchMeals();
  }

  getTotalCals(){
    this.cal = 0;
      this.meals.forEach(meal => {
        const newNumb = parseInt(String(meal.cal));
        this.cal+= newNumb;

      })
      return this.cal;
  }

  ngOnDestroy() {
    this.calorieChangeSubscription.unsubscribe();
    this.calorieFetchingSubscription.unsubscribe();
  }


}
